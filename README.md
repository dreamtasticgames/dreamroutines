﻿DreamRoutine
===
DreamRoutine exists to provide an solution to a number of questions regarding the inefficiencies of Coroutines.

+ Why do Coroutines incur a large amount garbage allocation per frame?
+ Why do Coroutines incur a large amount of cpu overhead?
+ Why can't Coroutines be checked for state?
+ Why can't Coroutines be paused?
+ Why can't Coroutines be resumed?

It's about time right?

Quick Start
---
---
Have you used Coroutines before? If so, then using DreamRoutine will feel very familiar. In fact, familiar enough to be integrated in games in all stages of development. We'll compare a simple usage pattern of the two systems below.

### The Coroutine usage pattern
Here we have a Coroutine that updates *idx* every frame. Pretty straightforward stuff.
```
using UnityEngine;
using System.Collections;

public class CoroutineBehavior : MonoBehaviour
{
    private int idx;

	private void Start()
    {
	   StartCoroutine(DoCoroutine());
	}

    private IEnumerator DoCoroutine()
    {
        while (true)
        {
	        idx++;
            yield return null;
        }
    }
}
```

### The DreamRoutine usage pattern
Now here we have we use DreamRoutine to update *idx* every frame.  Not much to it at all. The usage differences are noted inline.
```
using UnityEngine;
using System.Collections.Generic;

// NOTE: Utilize the Dreamtastic namespace
using Dreamtastic;

public class DreamRoutineBehavior : MonoBehaviour
{
    private int idx;

	private void Start()
    {
	    // NOTE: DreamRoutine.StartRoutine replaces StartCoroutine.
		// Additionally, the first argument is a gameObject to
		// represent the routine. The Coroutine standard is
		// Monobehaviour.gameObject.
	    DreamRoutine.StartRoutine(gameObject, DoDreamRoutine());
	}

	// NOTE: DRInstruction to be the IEnumerator return value.
    private IEnumerator<DRInstruction> DoDreamRoutine()
	{
		while(true)
		{
			index++;
			yield return null;
		}
	}
}
```

Performance
---
---
Coroutines are a fantastic mechanism that allow developers a different way to architect their game. They can be utilized for simple tasks such as long polling. They can also be used as complex state machines. Whatever the case, not all code is suitable for the Update function. In your own experience, *can you recall an Update function with over 5 different responsibilities, or that manages 5 different states,  or how about both at the same time?*

Coroutines are often the answer, but they are not perfect. They create garbage allocations per frame, they can create performance bottlenecks, and they are often hard to track. DreamRoutine serves to fix these inefficiencies so you can utilize the functionality of Coroutines without regret.

### The "10000 Call Test" Profiled

The test was run on 2 iOS devices compiled to IL2CPP in Release configuration. The time was measure as explained below.

 1. Set up a Stopwatch in the first Update called (configured in Script Execution Order)
 2. Stop the Stopwatch at LateUpdate
 3. Average the timings over exactly one minute

Unity version: **5.3.4f1**
iOS version: **9.3.2**

| Method        | iPhone 5s   | iPhone 6s  |
| :-------      | :----       | :----      |
| DreamRoutine  | **2.99ms**  | **2.18ms** |
| Update        | 4.78ms      | 2.76ms     |
| Coroutine     | 26.62ms     | 21.44ms    |

> **Note:** Yes, DreamRoutine provides a Coroutine solution that is in fact more efficient than Update.

Docs
---
---
Prerequisites: **node.js**, **npm**
DreamRoutine documentation can be generated from the **Docs** directory by running the following.

 1. npm install
 2. node docs

Help
---
---
If you questions, concerns, or feature requests; please contact the developer at
steven@dreamtasticgames.com.