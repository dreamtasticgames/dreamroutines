﻿using UnityEngine;
using System.Collections.Generic;
using Dreamtastic;

public class DreamRoutineSimpleExample : MonoBehaviour
{
	public int count;
	private int _index;
	private DRId _routineId;

	private void Start()
	{
		_routineId = DreamRoutine.StartRoutine(gameObject, DoDreamRoutine());
	}

	private IEnumerator<IDRInstruction> DoDreamRoutine()
	{
		while(true)
		{
			yield return null;
			_index++;
		}
	}

	public void OnToggleDreamRoutine()
	{

	}
}
