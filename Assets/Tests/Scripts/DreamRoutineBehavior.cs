using UnityEngine;
using System.Collections.Generic;
using Dreamtastic;

public class DreamRoutineBehavior : MonoBehaviour
{
	public int count;
	private int index;
	private int state;
	private List<DRId> routinesIds = new List<DRId>();
	private DRTimedUpdate timedUpdate = new DRTimedUpdate(5);

	private void Start()
	{
		StartTest();
	}

	private void StartTest()
	{
		for (int idx = 0; idx < count; ++idx)
		{
			DRId routineId = DreamRoutine.StartRoutine(gameObject, DoDreamRoutine());
			routinesIds.Add(routineId);
		}
	}

	private IEnumerator<IDRInstruction> DoDreamRoutine()
	{
		while(true)
		{
			yield return null;
			index++;
		}
	}

	public void OnTest()
	{
		if (state == 0)
		{
			Debug.Log("state : pause by idx");
			++state;
			for (int idx = 0; idx < routinesIds.Count; ++idx)
			{
				DreamRoutine.PauseRoutine(routinesIds[idx]);
			}
		}
		else if (state == 1)
		{
			Debug.Log("state : resume by idx");
			++state;
			for (int idx = 0; idx < routinesIds.Count; ++idx)
			{
				DreamRoutine.ResumeRoutine(routinesIds[idx]);
			}
		}
		else if (state == 2)
		{
			Debug.Log("state : pause by gameobject");
			++state;
			DreamRoutine.PauseRoutines(gameObject);
		}
		else if (state == 3)
		{
			Debug.Log("state : resume by gameobject");
			++state;
			DreamRoutine.ResumeRoutines(gameObject);
		}
		else if (state == 4)
		{
			Debug.Log("state : stop by gameobject");
			++state;
			DreamRoutine.StopRoutines(gameObject);
		}
		else
		{
			state = 0;
			Debug.Log("state : start test");
			StartTest();
		}
	}

	private void OnDestroy()
	{
		DreamRoutine.Reset();
	}
}
