﻿using UnityEngine;
using System.Collections;

public class CoroutineManager : MonoBehaviour {

    public int test;
    private int index;
	private void Start()
    {
        for (int idx = 0; idx < test; ++idx)
        {
            StartCoroutine(MyUpdate());
        }
	}

    private IEnumerator MyUpdate()
    {
        while (true)
        {
            index++;
            yield return null;
        }
    }
}