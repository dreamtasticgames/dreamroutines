using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrinaryCoroutineBehavior : MonoBehaviour {

    public int Total;
    private int i;

    private void Start()
    {
        for (var i = 0; i < Total; i++)
        {
            MovementEffects.Timing.RunCoroutine(MyUpdate());
        }
    }

    private IEnumerator<float> MyUpdate()
    {
        while (true)
        {
            yield return 0f;
            i++;
        }
    }
}
